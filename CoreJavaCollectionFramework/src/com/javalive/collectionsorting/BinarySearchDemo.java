package com.javalive.collectionsorting;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

class Country implements Comparable<Country> {
	private String name;

	public Country(String name) {
		super();
		this.name = name;
	}

	@Override
	public String toString() {
		return "Country [name=" + name + "]";
	}

	@Override
	public int compareTo(Country obj) {
		return this.name.compareTo(obj.name);
	}

}

public class BinarySearchDemo {
	public static void main(String[] args) {
		List<Country> countryList = new ArrayList<Country>();
		for (int i = 10; i > 0; i--) {
			Country obj = new Country("name" + i);
			countryList.add(obj);
		}
		for (int i = 0; i < 10; i++) {
			System.out.println(countryList.get(i).toString());
		}
		System.out.println("Performing binary search before sorting the list....");
		System.out.println("Element name4 is present in the given list at position "+(Collections.binarySearch(countryList, new Country("name4"))+1));
		System.out.println("Please note that we have not got the element name4 in the given list even if it is present in the list."
				+ "\nThis is because we have performed the binary search on unsorted list.");
		System.out.println("Applying the sort.");
		Collections.sort(countryList);
		System.out.println("Sorted list....");
		for (int i = 0; i < 10; i++) {
			System.out.println(countryList.get(i).toString());
		}
		System.out.println("Please note that this is list of string objects hence name10 is at second position.");
		System.out.println("Now performing binary search after sorting the list....");
		System.out.println("Element name4 is present in the given list at position "+(Collections.binarySearch(countryList, new Country("name4"))+1));
		System.out.println("Now we have got the desired output as the list is sorted.");
		System.out.println("So the moral of the story is, to apply the binary search for the given Collections object,"
				+ "it should be sorted first.");
	}
}
