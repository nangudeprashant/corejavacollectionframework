package com.javalive.equalsandhashcode;

//class with default equals()
//We define a class called Student as the following:
class Student1 {
	private int id;
	private String name;

	public Student1(int id, String name) {
		this.name = name;
		this.id = id;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}

// For testing purposes, we define a main class that checks
// whether two instances of Student (who have the exact same attributes) are
// considered as equal.
public class EqualsAndHashCodeDemoPart1 {
	public static void main(String[] args) {
		Student1 alex1 = new Student1(1, "Alex");
		Student1 alex2 = new Student1(1, "Alex");
		System.out.println("alex1 hashcode = " + alex1.hashCode());
		System.out.println("alex2 hashcode = " + alex2.hashCode());
		System.out.println("Checking equality between alex1 and alex2 = " + alex1.equals(alex2));
	}
}

/*
 * output of the program is: 
 * alex1 hashcode = 2018699554 alex2 hashcode =1311053135 
 * Checking equality between alex1 and alex2 = false
 * Explanation of the output:
 * Although the two instances have exactly the same attribute values, they are
 * stored in different memory locations. Hence, they are not considered equal as
 * per the default implementation of equals(). The same applies for hashcode() �
 * a random unique code is generated for each instance.
 */