package com.javalive.equalsandhashcode;

import java.util.ArrayList;
import java.util.List;

/*
 *  Overriding equals()
 *  For business purposes, we consider that two students are equal if they have the same ID,
 *  so we override the equals() method and provide our own implementation as the following:
*/
class Student2 {
	private int id;
	private String name;

	public Student2(int id, String name) {
		this.name = name;
		this.id = id;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		if (!(obj instanceof Student2))// real time use of instanceOf operator
			return false;
		if (obj == this)// real time use of this
			return true;
		return this.getId() == ((Student2) obj).getId();// real time use of type conversion
	}
}

public class EqualsAndHashCodeDemoPart2 {
	public static void main(String[] args) {
		Student2 alex1 = new Student2(1, "Alex");
		Student2 alex2 = new Student2(1, "Alex");
		System.out.println("alex1 hashcode = " + alex1.hashCode());
		System.out.println("alex2 hashcode = " + alex2.hashCode());
		System.out.println("Checking equality between alex1 and alex2 = " + alex1.equals(alex2));
	}
}
/*
 * In the above implementation, we are saying that two students are equal if and
 * only if they are stored in the same memory address OR they have the same ID.
 */
/*
 * output of the above program will be: 
 * alex1 hashcode = 2032578917 
 * alex2 hashcode = 1531485190 
 * Checking equality between alex1 and alex2 = true
 * Explanation of the output: As you noticed, overriding equals() with our
 * custom business forces Java to consider the ID attribute when comparing two
 * Student objects.
 */
