package com.javalive.equalsandhashcode;

import java.util.HashSet;
/*
 * equals() With HashSet and with overridden hashCode()
*/

class Student5 {
	private int id;
	private String name;

	public Student5(int id, String name) {
		this.name = name;
		this.id = id;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		if (!(obj instanceof Student5))
			return false;
		if (obj == this)
			return true;
		return this.getId() == ((Student5) obj).getId();
	}

	@Override
	public int hashCode() {
		return id * 13;
	}
}

public class EqualsAndHashCodeDemoPart5 {
	public static void main(String[] args) {
		Student5 alex1 = new Student5(1, "Alex");
		Student5 alex2 = new Student5(1, "Alex");

		HashSet<Student5> students = new HashSet<Student5>();
		students.add(alex1);
		students.add(alex2);
		System.out.println("HashSet size = " + students.size());
		System.out.println("HashSet contains Alex = " + students.contains(new Student5(1, "Alex")));
	}
}
