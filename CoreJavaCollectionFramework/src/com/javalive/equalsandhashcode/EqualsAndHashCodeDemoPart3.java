package com.javalive.equalsandhashcode;

import java.util.ArrayList;
import java.util.List;

/*
 * equals() With ArrayList:-
 * A very popular usage of equals() is defining an array list of Student and searching for a particular
 * student inside it. So we modified our previous class in order the achieve this.
*/
class Student3 {
	private int id;
	private String name;

	public Student3(int id, String name) {
		this.name = name;
		this.id = id;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		if (!(obj instanceof Student3))// real time use of instanceOf operator
			return false;
		if (obj == this)// real time use of this
			return true;
		return this.getId() == ((Student3) obj).getId();// real time use of type conversion
	}
}

public class EqualsAndHashCodeDemoPart3 {
	public static void main(String[] args) {
		Student3 alex1 = new Student3(1, "Alex");
		Student3 alex2 = new Student3(1, "Alex");
		System.out.println("alex1 hashcode = " + alex1.hashCode());
		System.out.println("alex2 hashcode = " + alex2.hashCode());
		System.out.println("Checking equality between alex1 and alex2 = " + alex1.equals(alex2));

		List<Student3> studentsLst = new ArrayList<Student3>();
		studentsLst.add(alex1);
		System.out.println("Arraylist size = " + studentsLst.size());
		System.out.println("Arraylist contains Alex = " + studentsLst.contains(new Student3(1, "Alex")));
	}
}

