package com.javalive.equalsandhashcode;

import java.util.ArrayList;

class Player {
	private String name;

	public Player(String name) {
		super();
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Player other = (Player) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}
}

public class EqualsAndHashCodeBasic {
	public static void main(String[] args) {
		ArrayList<Player> playerList = new ArrayList<Player>();
		Player obj1 = new Player("Alex");// for understanding purpose value for hashcode bucket is (e.g.a+l+e+x=42):42
		playerList.add(obj1);
		Player obj2 = new Player("Amy");// Value is :31
		playerList.add(obj2);
		Player obj3 = new Player("Bob");// Value is :19
		playerList.add(obj3);
		Player obj4 = new Player("Dirk");// Value is :42
		playerList.add(obj4);
		Player obj5 = new Player("Fred");// Value is :33
		playerList.add(obj5);
		Player obj6 = new Player("May");// Value is :31
		playerList.add(obj6);
		Player obj7 = new Player("Alex");// Value is :42
		playerList.add(obj7);
		System.out.println(obj1.equals(obj7));//hashcode and equals both are true
		System.out.println(obj1.equals(obj4));//hashcode true but equals false
		System.out.println(obj2.equals(obj6));//hashcode true but equals false. **Please note that hascode false but equals true is not possible in any circumstances. 
		System.out.println(obj1.equals(obj6));//both hashcode and equals are false
	}
}