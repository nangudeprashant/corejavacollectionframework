package com.javalive.collectioninterfacesandclasses;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class CollectionsClassDemo {
	public void disJointDemo() {
		// create list1
		List<String> list1 = new ArrayList<>();

		// add elements to list1
		list1.add("Shoes");
		list1.add("Toys");
		list1.add("Horse");
		list1.add("Tiger");

		// create list2
		List<String> list2 = new ArrayList<>();

		// add elements to list2
		list2.add("Bat");
		list2.add("Frog");
		list2.add("Lion");

		// check if disjoint or not
		System.out.println(Collections.disjoint(list1, list2));

	}

	public void copyCollectionDemo() {
		// create destination list
		List<String> destination_List = new ArrayList<>();

		// add elements
		destination_List.add("Shoes");
		destination_List.add("Toys");
		destination_List.add("Horse");
		destination_List.add("Tiger");

		// print the elements
		System.out.println("The Original Destination list is ");
		for (int i = 0; i < destination_List.size(); i++) {
			System.out.print(destination_List.get(i) + " ");
		}
		System.out.println();

		// create source list
		List<String> source_List = new ArrayList<>();
		source_List.add("Bat");
		source_List.add("Frog");
		source_List.add("Lion");

		// copy the elements from source to destination
		Collections.copy(destination_List, source_List);

		// printing the modified list
		System.out.println("The Destination List After copying is ");
		for (int i = 0; i < destination_List.size(); i++) {
			System.out.print(destination_List.get(i) + " ");
		}

	}

	public void binarySearchOnCollection() {
		// create a list
		List<String> items = new ArrayList<>();

		// Add elements
		items.add("Shoes");
		items.add("Toys");
		items.add("Horse");
		items.add("Ball");
		items.add("Grapes");

		// sort the list
		Collections.sort(items);

		// binarySearch on the list
		System.out.println("The index of Horse is " + Collections.binarySearch(items, "Horse"));

		// binarySearch on the list
		System.out.println("The index of Dog is " + Collections.binarySearch(items, "Dog"));
	}

	public void collectionSortingDemo() {
		// create a list
		List<String> items = new ArrayList<>();

		// add elements to the list
		items.add("Shoes");
		items.add("Toys");

		// add one or more elements
		Collections.addAll(items, "Fruits", "Bat", "Mouse");

		// sorting according to default ordering
		Collections.sort(items);

		// print the elements
		for (int i = 0; i < items.size(); i++) {
			System.out.print(items.get(i) + " ");
		}
		System.out.println();

		// sorting according to reverse ordering
		Collections.sort(items, Collections.reverseOrder());

		// print the reverse order
		for (int i = 0; i < items.size(); i++) {
			System.out.print(items.get(i) + " ");
		}
	}
	
	public static void main(String[] args) {
		CollectionsClassDemo obj=new CollectionsClassDemo();
		System.out.println("======binarySearchOnCollection()============");
		obj.binarySearchOnCollection();
		System.out.println("======collectionSortingDemo()===============");
		obj.collectionSortingDemo();
		System.out.println("\n======copyCollectionDemo()================");
		obj.copyCollectionDemo();
		System.out.println("\n======disJointDemo()======================");
		obj.disJointDemo();
		System.out.println("============================================");
	}
}


