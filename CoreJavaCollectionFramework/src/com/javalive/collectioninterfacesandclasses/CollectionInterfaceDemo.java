package com.javalive.collectioninterfacesandclasses;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author www.itaspirants.in
 * @description The Java Collection interface (java.util.Collection) is one of
 *              the root interfaces of the Java Collection API. Though you do
 *              not instantiate a Collection directly, but rather a sub type of
 *              Collection, you may often treat these sub types uniformly as a
 *              Collection. In this program you will see how:
 *
 */
public class CollectionInterfaceDemo {
	public static void main(String[] args) {
	      // ArrayList 
	      List a1 = new ArrayList();
	      a1.add("ABC1");
	      a1.add("PQR1");
	      a1.add("XYZ1");
	      System.out.println("ArrayList Elements  "+a1);
	      System.out.println("First element viz "+a1.remove(0)+" is removed from the given ArrayList.");
	      System.out.println("Updated ArrayList Elements   "+a1);
	      // LinkedList
	      System.out.println("==============================================");
	      List l1 = new LinkedList();
	      l1.add("ABC2");
	      l1.add("PQR2");
	      l1.add("XYZ2");
	      System.out.println("LinkedList Elements  "+l1);
	      System.out.println("Getting first element of the list "+l1.get(0));
	      System.out.println("==============================================");
	      // HashSet
	      Set s1 = new HashSet(); 
	      s1.add("ABC3");
	      s1.add("PQR3");
	      s1.add("XYZ3");
	      System.out.println("Is given set contains ABC3 "+s1.contains("ABC3"));
	      System.out.println("Set Elements  "+s1);
	      System.out.println("==============================================");
	      // HashMap
	      Map m1 = new HashMap(); 
	      m1.put("Zara", "8");
	      m1.put("Mahnaz", "31");
	      m1.put("Ayan", "12");
	      m1.put("Daisy", "14");
	      System.out.println("Map Elements  "+m1);
	      System.out.println("Replacing value of Ayan element....");
	      m1.replace("Ayan", 10);
	      System.out.println("Updated Map Elements  "+m1);
	      System.out.println("==============================================");
	   }
}
